import json
import uuid

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import Bid, SearchBid
from .serializers import BidSerializer


@api_view(['GET'])
def bids_list(request):
    data = []
    nextPage = 1
    previousPage = 1
    bids = Bid.objects.all().order_by('-received')
    page = request.GET.get('page', 1)
    paginator = Paginator(bids, 20)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    serializer = BidSerializer(data, context={'request': request}, many=True)
    if data.has_next():
        nextPage = data.next_page_number()
    if data.has_previous():
        previousPage = data.previous_page_number()

    return Response({'data': serializer.data, 'count': paginator.count, 'numpages': paginator.num_pages,
                     'nextlink': '/api/bids/?page=' + str(nextPage),
                     'prevlink': '/api/bids/?page=' + str(previousPage)})
