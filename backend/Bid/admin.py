from django.contrib import admin

# Register your models here.
from .models import Bid

admin.site.register(Bid)
