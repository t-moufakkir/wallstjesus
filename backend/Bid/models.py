from django.utils import timezone
from django.db import models


class Bid(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    received = models.DateTimeField(default=timezone.now)
    sym = models.CharField(max_length=50)
    expires = models.CharField(max_length=50)
    strike = models.CharField(max_length=50)
    ref = models.CharField(max_length=50)
    type = models.CharField(max_length=50)
    size = models.CharField(max_length=50)
    sentiment = models.CharField(max_length=50)
    t = models.IntegerField()


class SearchBid(models.Model):
    search_id = models.CharField(max_length=50)
    id = models.CharField(max_length=50, primary_key=True)
    received = models.DateTimeField(default=timezone.now)
    sym = models.CharField(max_length=50)
    expires = models.CharField(max_length=50)
    strike = models.CharField(max_length=50)
    ref = models.CharField(max_length=50)
    type = models.CharField(max_length=50)
    size = models.CharField(max_length=50)
    sentiment = models.CharField(max_length=50)
    t = models.IntegerField()