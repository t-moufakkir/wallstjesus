from rest_framework import serializers
from .models import Bid


class BidSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bid
        fields = ('pk', 'received', 'sym', 'expires', 'strike', 'ref', 'type', 'size', 'sentiment')
