# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class BidItem(scrapy.Item):
    # define the fields for your item here like:
    received = scrapy.Field()
    sym = scrapy.Field()
    expires = scrapy.Field()
    strike = scrapy.Field()
    ref = scrapy.Field()
    type = scrapy.Field()
    size = scrapy.Field()
    sentiment = scrapy.Field()
    pass


class SearchBidItem(scrapy.Item):
    # define the fields for your item here like:
    search_id = scrapy.Field()
    received = scrapy.Field()
    sym = scrapy.Field()
    expires = scrapy.Field()
    strike = scrapy.Field()
    ref = scrapy.Field()
    type = scrapy.Field()
    size = scrapy.Field()
    sentiment = scrapy.Field()
    pass
