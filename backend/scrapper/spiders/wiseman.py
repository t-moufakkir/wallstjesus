from scrapy import Spider, Request
from datetime import timedelta, datetime
from hashlib import md5
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome import webdriver
from time import sleep
from pytz import timezone

from Bid.models import Bid
from ..items import BidItem


class WiseMan(Spider):
    name = 'wiseman'

    dashboard_url = 'https://app.wallstjesus.com/wiseguy'

    login_url = 'https://app.wallstjesus.com/login'
    username = 'TishaEF'
    password = 'Trade.2021'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        options.add_argument('start-maximized')
        options.add_argument("disable-infobars")
        options.add_argument("--disable-extensions")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--no-sandbox")
        self.driver = webdriver.WebDriver(chrome_options=options)
        self.login()

    def login(self):
        self.driver.get(self.login_url)
        WebDriverWait(self.driver, 40).until(EC.presence_of_element_located((By.ID, 'loginId')))
        username_input = self.driver.find_element_by_id('loginId')
        username_input.send_keys(self.username)
        password_input = self.driver.find_element_by_id('loginPassword')
        password_input.send_keys(self.password)
        submit_button = self.driver.find_element_by_id('loginButton')
        submit_button.click()
        current_url = self.driver.current_url
        try:
            WebDriverWait(self.driver, 20).until(EC.url_changes(current_url))
        except NoSuchElementException:
            self.login()

    def start_requests(self):
        self.driver.get(self.dashboard_url)
        yield Request('data:,Hello%2C%20World%21', callback=self.parse, dont_filter=True)

    def parse(self, response, **kwargs):
        WebDriverWait(self.driver, 40).until(EC.presence_of_element_located((By.CLASS_NAME, 'MuiTable-root')))
        sleep(5)
        last_bid = False
        while True:
            trs = self.driver.find_elements_by_tag_name('tr')
            today = datetime.now(timezone('US/Eastern'))
            t = today.strftime("%m/%d/%Y")
            for tr in trs:
                self.driver.execute_script("arguments[0].scrollIntoView();", tr)
                tds = tr.find_elements_by_tag_name('td')
                if len(tds) == 1:
                    t = tds[0].text
                    if t == 'TODAY':
                        today = datetime.now(timezone('US/Eastern'))
                        t = today.strftime("%m/%d/%Y")
                    if t == 'YESTERDAY':
                        yesterday = datetime.now(timezone('US/Eastern')) - timedelta(days=1)
                        t = yesterday.strftime("%m/%d/%Y")
                elif len(tds) > 9:
                    item = BidItem()
                    item['received'] = tds[0].text + ' ' + t
                    item['sym'] = tds[1].text
                    item['expires'] = tds[2].text.replace(' ', ':')
                    if len(item['expires']) < 8:
                        item['expires'] = str(datetime.now().year) + '-' + item['expires']
                    item['strike'] = tds[3].text
                    item['ref'] = tds[4].text
                    tmp = tds[5].find_element_by_tag_name('svg')
                    try:
                        n = tmp.find_element_by_tag_name('polygon')
                        item['type'] = 'SWEEP'
                    except NoSuchElementException:

                        n = tmp.find_element_by_tag_name('rect').get_attribute('style')
                        if n == 'fill: rgb(16, 120, 187);':
                            item['type'] = 'SLIP'
                        else:
                            item['type'] = 'BLOCK'
                    item['size'] = tds[6].text
                    item['sentiment'] = tds[7].text
                    hash = md5(str(item).encode()).hexdigest()
                    try:
                        bid = Bid.objects.get(id=hash)
                        last_bid = True
                        continue
                    except Bid.DoesNotExist:
                        last_bid = False
                        yield item

            if not last_bid:
                try:
                    more_button = self.driver.find_element_by_xpath('//*[@id="root"]/div/div[1]/div[3]/table/tbody/tr[22]/td/div/div/button')
                    self.driver.execute_script("arguments[0].scrollIntoView();", more_button)
                    more_button.click()
                    sleep(3)
                except NoSuchElementException:
                    pass
