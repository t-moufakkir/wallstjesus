import time
from datetime import datetime

from Bid.models import Bid, SearchBid
from hashlib import md5

from .items import BidItem, SearchBidItem


class ScrapperPipeline:
    def process_item(self, item, spider):
        if isinstance(item, BidItem):
            hash = md5(str(item).encode()).hexdigest()
            try:
                bid = Bid.objects.get(id=hash)
                return item
            except Bid.DoesNotExist:
                pass
            bid = Bid()
            bid.id = hash
            bid.received = datetime.strptime(item['received'], '%H:%M %m/%d/%Y')
            bid.sym = item['sym']
            bid.expires = item['expires']
            bid.strike = item['strike']
            bid.ref = item['ref']
            bid.type = item['type']
            bid.size = item['size']
            bid.sentiment = item['sentiment']
            bid.t = int(time.time())
            bid.save()
            return item

        elif isinstance(item, SearchBidItem):
            hash = md5(str(item).encode()).hexdigest()
            try:
                bid = SearchBid.objects.get(id=hash)
                return item
            except SearchBid.DoesNotExist:
                pass
            bid = SearchBid()
            bid.id = hash
            bid.search_id = item['search_id']
            bid.received = datetime.strptime(item['received'], '%H:%M %m/%d/%Y')
            bid.sym = item['sym']
            bid.expires = item['expires']
            bid.strike = item['strike']
            bid.ref = item['ref']
            bid.type = item['type']
            bid.size = item['size']
            bid.sentiment = item['sentiment']
            bid.t = int(time.time())
            bid.save()
            return item